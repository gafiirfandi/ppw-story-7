from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect, HttpResponseRedirect
from .models import HelloApaKabar
from . import forms



# Create your views here.
def home(request):
    if request.method == 'POST':
        form = forms.HelloApaKabarForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('home')
    else:
        form = forms.HelloApaKabarForm()
    return render(request, 'home.html', {'form':form, 'list_status': HelloApaKabar.objects.all().order_by('-date')})

def searchbook(request):
    return render (request, 'searchbook.html')