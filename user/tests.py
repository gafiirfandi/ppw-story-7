from django.test import Client, TestCase

# Create your tests here.
class UnitTest(TestCase):

    def test_response(self):
        response = Client().get('/user/login')
        self.assertEqual(response.status_code, 200)

    def test_response_logout(self):
        response = Client().get('/user/logout')
        self.assertEqual(response.status_code, 200)
    
    def test_utt_GET(self):
        response = self.client.get('/user/login')
        self.assertTemplateUsed(response, 'login.html')

    def test_utt_logout_GET(self):
        response = self.client.get('/user/logout')
        self.assertTemplateUsed(response, 'logout.html')
        