from django.shortcuts import render, redirect
from django.views.generic import TemplateView

from django.contrib.auth import authenticate, login, logout

# Create your views here.
def loginView(request):
	context = {
		'page_title':'LOGIN',
	}
	user = None
	if request.method == "POST":
		
		username_login = request.POST['username']
		password_login = request.POST['password']
		
		user = authenticate(request, username=username_login, password=password_login)

		if user is not None:
			login(request, user)
			request.session["username"] = user.username
			return redirect('home')
		else:
			return redirect('login')
		
	return render(request, 'login.html', context)


def logoutView(request):
	request.session.flush()
	context = {
		'page_title':'logout'
	}

	if request.method == "POST":
		if request.POST["logout"] == "Submit":
			logout(request)
			

		return redirect('home')	


	return render(request, 'logout.html', context)